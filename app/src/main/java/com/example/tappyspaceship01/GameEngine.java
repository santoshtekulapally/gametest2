package com.example.tappyspaceship01;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    Bitmap Item1image;
    Bitmap Item2mage;
    Bitmap Item3image;
    Bitmap Item4image;

    int Item1Xposition;
    int Item1Yposition;
    Rect Item1HitBox;

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    int screenHeight;
    int screenWidth;


    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    Player player;
    Item Item1;
    Item Item2;
    Item Item3;
    Item Item4;


    Bitmap background;
    int bgXPosition = 0;
    int backgroundRightSide = 0;
    // ----------------------------
    // ## GAME STATS
    // ----------------------------

    int lives = 3;
    int Scorecount = 0;

    int SQUARE_WIDTH = 25;

//added by santosh//

    public GameEngine(Context context, int w, int h) {
        super(context);


//added by santosh//
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = w;
        this.screenHeight = h;

        // make bullets


        // make bullets

        // initalize enemy
        // this.player = new Square(context, 1000, 500, 100);

        // added code bvy santosh


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        this.Item1image = BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.poop32);


        // @TODO: Add your sprites








        // put the initial starting position of your player and enemies
        this.player = new Player(getContext(), 1500, 250);
        this.Item1 = new Item(getContext(), 0, 100);
        this.Item2 = new Item(getContext(), 110, 600);
        this.Item3 = new Item(getContext(), 150, 330);
        this.Item4 = new Item(getContext(), 70, 800);

        // setup the background

        this.Item2mage = BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.rainbow32);
       this.Item2.setImage(Item2mage);



        this.Item3image = BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.poop32);

        this.Item3.setImage(Item3image);


        this.Item4image = BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.candy32);

        this.Item4.setImage(Item4image);

//Here we are setting the image in image view
      //  img.setImageResource(R.drawable.my_image);

        this.background = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        // dynamically resize the background to fit the device
        this.background = Bitmap.createScaledBitmap(
                this.background,
                this.screenWidth,
                this.screenHeight,
                false
        );

        this.bgXPosition = 0;

    }


    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }



    private void spawnEnemyShips() {

        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    int numLoops = 0;


    public void updatePositions() {







        if (this.player.getyPosition() <= this.screenHeight) {


             // this.player.setyPosition(00);

        }



 if (this.Item1.getxPosition() >= this.screenWidth) {

     Log.d(TAG, "++++++item crossing the screen");
//remove the item code shpuld be writeen here


        }
      else  if (this.Item2.getxPosition() >= this.screenWidth) {

            Log.d(TAG, "++++++item crossing the screen");
//remove the item code shpuld be writeen here


        }
      else  if (this.Item3.getxPosition() >= this.screenWidth) {

            Log.d(TAG, "++++++item crossing the screen");
//remove the item code shpuld be writeen here


        }
       else if (this.Item4.getxPosition() >= this.screenWidth) {

            Log.d(TAG, "++++++item crossing the screen");
//remove the item code shpuld be writeen here


        }


        // UPDATE BACKGROUND POSITION
        // 1. Move the background
        this.bgXPosition = this.bgXPosition - 50;

        backgroundRightSide = this.bgXPosition + this.background.getWidth();
        // 2. Background collision detection
        if (backgroundRightSide < 0) {
            this.bgXPosition = 0;
        }

        numLoops = numLoops + 1;

        // @TODO: Update position of player based on mouse tap
        if (this.fingerAction == "mousedown") {
            // if mousedown, then move player up
            // Make the UP movement > than down movement - this will
            // make it look like the player is moving up alot

//              this.player.setxPosition(1500);
//              this.player.setyPosition(600);

            //added code set
            player.setyPosition(player.getyPosition() - 50);
            player.updateHitbox();

        } else if (this.fingerAction == "mouseup") {
            // if mouseup, then move player down
            player.setyPosition(player.getyPosition() + 50);


            player.updateHitbox();
        }

        // MAKE ENEMY MOVE
        // - enemy moves left forever
        // - when enemy touches LEFT wall, respawn on RIGHT SIDE
       // this.enemy1.setyPosition(this.enemy1.getyPosition() - 25);
        this.Item1.setxPosition(this.Item1.getxPosition()+15);

        this.Item2.setxPosition(this.Item2.getxPosition()+15);

        this.Item3.setxPosition(this.Item3.getxPosition()+10);

        this.Item4.setxPosition(this.Item4.getxPosition()+5);


        // MOVE THE HITBOX (recalcluate the position of the hitbox)
        this.Item1.updateHitbox();
        this.Item2.updateHitbox();
        this.Item3.updateHitbox();
        this.Item4.updateHitbox();

        if (this.Item1.getyPosition() <= 0) {
            // restart the enemy in the starting position
            this.Item1.setxPosition(1300);
            this.Item1.setyPosition(500);

            // this.enemy1.setyPosition(this.enemy1.getyPosition()+25);

            this.Item1.updateHitbox();
        }

       // this.enemy2.setyPosition(this.enemy2.getyPosition() - 25);

        // MOVE THE HITBOX (recalcluate the position of the hitbox)
        this.Item2.updateHitbox();

        if (this.Item2.getyPosition() <= 0) {
            // restart the enemy in the starting position


            this.Item2.setxPosition(1500);
            this.Item2.setyPosition(500);
            this.Item2.updateHitbox();
        }

        if (this.Item2.getyPosition() <= 0) {
            // restart the enemy in the starting position
            this.Item2.setxPosition(1300);
            this.Item2.setyPosition(120);
            //  this.enemy2.setyPosition(this.enemy2.getyPosition()+25);

            this.Item2.updateHitbox();
        }
        if (this.Item3.getyPosition() <= 0) {
            // restart the enemy in the starting position
            this.Item3.setxPosition(1300);
            this.Item3.setyPosition(120);
            //  this.enemy2.setyPosition(this.enemy2.getyPosition()+25);

            this.Item3.updateHitbox();
        }
        if (this.Item4.getyPosition() <= 0) {
            // restart the enemy in the starting position
            this.Item4.setxPosition(1300);
            this.Item4.setyPosition(120);
            //  this.enemy2.setyPosition(this.enemy2.getyPosition()+25);

            this.Item4.updateHitbox();
        }
        // DEAL WITH BULLETS





        // COLLISION DETECTION ON THE BULLET AND WALL
//        for (int i = 0; i < this.enemy1.getBullets().size(); i++) {
//            Rect bullet = this.enemy1.getBullets().get(i);
//
//            // For each bullet, check if teh bullet touched the wall
//            if (bullet.right < 0) {
//                this.enemy1.getBullets().remove(bullet);
//            }
//
//        }
//
//        // COLLISION DETECTION BETWEEN BULLET AND PLAYER
//        for (int i = 0; i < this.enemy1.getBullets().size(); i++) {
//            Rect bullet = this.enemy1.getBullets().get(i);
//
//            if (this.player.getHitbox().intersect(bullet)) {
//                this.player.setxPosition(100);
//                this.player.setyPosition(600);
//                this.player.updateHitbox();
//                lives = lives - 1;
//            }
//
//        }


        // MAKE ENEMY2 MOVE
        // MAKE ENEMY MOVE
        // - enemy moves left forever
        // - when enemy touches LEFT wall, respawn on RIGHT SIDE
        //this.enemy2.setxPosition(this.enemy2.getxPosition());


        // @TODO:  Check collisions between enemy and player
        if (this.player.getHitbox().intersect(this.Item1.getHitbox()) == true) {
            // the enemy and player are colliding
            Log.d(TAG, "++++++ENEMY AND PLAYER COLLIDING!");



            Log.d(TAG, "Colleied with poooooopooooooooooooop againnnnnnnnnnn");

            Log.d(TAG, "Colleied with rainbow..! wow thts a good day");

            // decrease the lives
            lives = lives - 1;

            if (lives <= 0) {



                this.pauseGame();

            }

        }
        if (this.player.getHitbox().intersect(this.Item2.getHitbox()) == true) {
            // the enemy and player are colliding


            // @TODO: What do you want to do next?



            Scorecount = Scorecount +1;

            // decrease the lives


        }
        if (this.player.getHitbox().intersect(this.Item3.getHitbox()) == true) {
            // the enemy and player are colliding

            Log.d(TAG, "Colleied with pooop omg! died ");

            // @TODO: This is a poop


            lives = lives - 1;

            if (lives <= 0) {



                this.pauseGame();

            }

        }


        // @TODO:  Check collisions between item and player
        if (this.player.getHitbox().intersect(this.Item4.getHitbox()) == true) {
            // the enemy and player are colliding
            Log.d(TAG, "++++++ENEMY 2 AND PLAYER COLLIDING!");

            Log.d(TAG, "Colleied with candy !! its tassty nowwwww ");


            Scorecount = Scorecount +1;

        }
// added by san

    }

//added b san //




    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------



            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.YELLOW);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            // DRAW THE BACKGROUND
            // -----------------------------
            canvas.drawBitmap(this.background,
                    this.bgXPosition,
                    0,
                    paintbrush);

            canvas.drawBitmap(this.background,
                    backgroundRightSide,
                    0,
                    paintbrush);



            // draw player graphic on screen
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(Item1.getImage(), Item1.getxPosition(), Item1.getyPosition(), paintbrush);
            // 2. draw the enemy's hitbox
            canvas.drawRect(Item1.getHitbox(), paintbrush);


            // draw enemy 2 on the screen
            // draw the enemy graphic on the screen
            canvas.drawBitmap(Item2.getImage(), Item2.getxPosition(), Item2.getyPosition(), paintbrush);
            // 2. draw the enemy's hitbox
            canvas.drawRect(Item2.getHitbox(), paintbrush);

            canvas.drawBitmap(Item3.getImage(), Item3.getxPosition(), Item3.getyPosition(), paintbrush);
            // 2. draw the enemy's hitbox
            canvas.drawRect(Item3.getHitbox(), paintbrush);


            canvas.drawBitmap(Item4.getImage(), Item4.getxPosition(), Item4.getyPosition(), paintbrush);
            // 2. draw the enemy's hitbox
            canvas.drawRect(Item4.getHitbox(), paintbrush);
//            for (int i = 0; i < this.player.getBullets().size(); i++) {
//                Rect bullet = this.player.getBullets().get(i);
//                canvas.drawRect(bullet, paintbrush);
//            }
//            // draw bullet on screen
//            for (int i = 0; i < this.Item1.getBullets().size(); i++) {
//                Rect bullet = this.Item1.getBullets().get(i);
//                canvas.drawRect(bullet, paintbrush);
//            }
////added by san
//            for (int i = 0; i < this.Item2.getBullets().size(); i++) {
//                Rect bullet = this.Item2.getBullets().get(i);
//                canvas.drawRect(bullet, paintbrush);
//            }
//
////added by san


            // DRAW GAME STATS
            // -----------------------------
            paintbrush.setColor(Color.BLACK);
            paintbrush.setTextSize(60);
            canvas.drawText("Lives =  " + lives,
                    500,
                    100,
                    paintbrush
            );


            canvas.drawText("Score = " + Scorecount,
                    1100,
                    100,
                    paintbrush

            );





            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }






    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        // Answer - PRESS DOWN ON SCREEN --> PLAYER MOVES UP
        // RELEASE FINGER --> PLAYER MOVES DOWN


        if (userAction == MotionEvent.ACTION_DOWN) {
            //Log.d(TAG, "Person tapped the screen");
            // User is pressing down, so move player up
            fingerAction = "mousedown";
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            //Log.d(TAG, "Person lifted finger");
            // User has released finger, so move player down
            fingerAction = "mouseup";
        }

        return true;
    }
}

